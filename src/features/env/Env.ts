import Constants from 'expo-constants'

interface EnvJson {
    [key:string]: any
}

export default class Env {

    static _mode : string = "dev"

    static _config : EnvJson

    static async load() : Promise<void> { 
        const extra = Constants.manifest.extra
        if(extra === undefined) {
            throw "no 'extra' key defined in app.json!" 
        }

        // default mode is always dev
        Env._mode = extra.mode ? extra.mode : "dev"
        let envJson : EnvJson

        if(extra.mode == "dev") {
            envJson = await require("./../../../env.dev.json")
        } else {
            envJson = await require("./../../../env.prod.json")
        }
        Env._config = envJson
    }

    static get(key : string) : any {
        let el = Env._config[key]
        if(el === undefined) {
            throw `The key ${key} doesn't exists in the env.${Env._mode}.json`
        }

        return el
    }
}