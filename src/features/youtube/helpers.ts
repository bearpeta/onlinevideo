import { PlaylistResponse, PlaylistItemResponse, ListResponse } from "./api/types";
import { Playlist, PlaylistItem, YTResponse } from "./types";

const setValue = (value: any, defaultValue: any) => {
    return (value === undefined) ? defaultValue : value;
}

export const createYTPlaylistResponse = (playlists : Playlist[], respData : ListResponse<PlaylistResponse>, channelId : string, notModified : boolean) : YTResponse<Playlist[]> => ({
    notModified,
    response: playlists,
    metaData: {
        etag : setValue(respData.etag, ''),
        channelId: setValue(channelId, ""),
        kind : "playlists",
        nextPageToken : setValue(respData.nextPageToken, ""),
        prevPageToken : setValue(respData.prevPageToken, ""),
        pageInfo : { 
            totalResults : setValue((respData.pageInfo || {}).totalResults, 0), 
            resultsPerPage: setValue((respData.pageInfo || {}).resultsPerPage, 0)
        }
    }

})

export const transformPlaylistResponse = (playlistResp : PlaylistResponse, metaId : string) : Playlist => ({
    etag: setValue(playlistResp.etag, ""),
    id: setValue(playlistResp.id, ""),
    metaId: setValue(metaId, ""),
    channelId: setValue(playlistResp.channelId, ""),
    title: setValue(playlistResp.title, ""),
    description: setValue(playlistResp.description, ""),
    publishedAt: setValue(playlistResp.publishedAt, ""),
    channelTitle: setValue(playlistResp.channelTitle, ""),
    thumbnails: setValue(playlistResp.thumbnails, {})
})

export const transformPlaylistItemResponse = (playlistResp : PlaylistItemResponse, respEtag: string) : PlaylistItem => ({
    etag: respEtag,
    id: playlistResp.id,
    playlistId: playlistResp.playlistId,
    channelId: playlistResp.channelId,
    publishedAt: playlistResp.publishedAt,
    title: playlistResp.title,
    description: playlistResp.description,
    channelTitle: playlistResp.channelTitle,
    position: playlistResp.position,
    thumbnails: playlistResp.thumbnails,
    videoId: playlistResp.resourceId.videoId,
})