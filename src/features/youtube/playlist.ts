import { ListOptions, ListResponse, ApiResponse, PlaylistResponse, IPlaylistApi } from "./api/types";
import { Playlist, YTResponse } from "./types";
import { transformPlaylistResponse, createYTPlaylistResponse } from "./helpers";
import { YTPlaylist } from ".";
import Env from "../env/Env";
import { createListResponse, createPlaylistResponse } from "./api/helpers";

export const getIdsFromChannels = async () : Promise<string[]> => {
    const channels : string[] = Env.get("YT_CHANNELS")

    const ytPromises : Promise<YTResponse<Playlist[]>[]>[] = []
    try {
        channels.forEach(channelId => {
        ytPromises.push(loadAllPlaylists({channelId: channelId, part: "id"}))
      })

    } catch (error) {
        console.log("getIdsFromChannels failed: " + error)
      return Promise.reject(error) 
    }

    const ytChannelResponses : YTResponse<Playlist[]>[][] = await Promise.all(ytPromises)
    const playlistIds : string[] = []
    ytChannelResponses.forEach((channelResponses : YTResponse<Playlist[]>[]) => {
      channelResponses.forEach((channelResp : YTResponse<Playlist[]>) => {
        channelResp.response.forEach(playlist => playlistIds.push(playlist.id))
      })
    })

    return playlistIds
}

const loadAllPlaylists = async (options : ListOptions) : Promise<YTResponse<Playlist[]>[]> => {
    let responses : YTResponse<Playlist[]>[] = []
    const snippet : YTResponse<Playlist[]> = await loadPlaylists(options)
    responses = responses.concat(snippet)
    if(snippet.metaData.nextPageToken != "") {
        options.pageToken = snippet.metaData.nextPageToken
        return responses.concat(await loadPlaylists(options))
    } else {
        return responses
    }
}

export const loadPlaylists = async (options : ListOptions) : Promise<YTResponse<Playlist[]>> => {
    const playlistApi : IPlaylistApi = YTPlaylist()
    const playlists : Playlist[] = []
    const channelId = options.channelId ? options.channelId : ""

    const jsonResponse : ApiResponse<ListResponse<PlaylistResponse>> = await playlistApi.list(options)

    if(jsonResponse.isError || jsonResponse.data === undefined) {
        
        if(jsonResponse.errMessage == "304") {
            return createYTPlaylistResponse(playlists, createListResponse({}, createPlaylistResponse), channelId, true)
        }

        return Promise.reject(jsonResponse.errMessage)
    }

    const notModified = jsonResponse.errMessage == "304"

    const respData : ListResponse<PlaylistResponse> = jsonResponse.data

    playlists.push(...jsonResponse.data.items.map<Playlist>(resp => transformPlaylistResponse(resp, respData.etag)))

    const result : YTResponse<Playlist[]>  = createYTPlaylistResponse(playlists, respData, channelId, notModified)

    return result
}

export const checkForPlaylistUpdate = async (currentPlaylists : Playlist[]) : Promise<Playlist[]> => {

    const plIds : string[] = await getIdsFromChannels()

    const newPlaylists : Playlist[] = []

    const newPlaylistIds : string[] = plIds.filter(newId => currentPlaylists.map(playlist => playlist.id).findIndex((plId) => plId == newId) == -1)
    if(newPlaylistIds.length > 0) {
        console.log("THERE ARE SOME NEW PLAYLISTS!!!!")
        const newPlPromises : Promise<YTResponse<Playlist[]>>[] = []
        newPlaylistIds.forEach(id => {
            try{
                newPlPromises.push(loadPlaylists({
                    part: "snippet",
                    id
                }))
            } catch(err) {
                console.log(`checkForPlaylistUpdate: Failed to load new playlist with ID #${id}: ${err}`)
                
            }
        })

        const newPlResponse : YTResponse<Playlist[]>[] = await Promise.all(newPlPromises)
        newPlResponse.forEach((resp : YTResponse<Playlist[]>) => newPlaylists.push(...resp.response))
    }

    const promises : Promise<YTResponse<Playlist[]>>[] = [] 
    const errors = []

    currentPlaylists.forEach(playlist => {
        try{
            promises.push(loadPlaylists({
                etag: playlist.metaId,
                part: "snippet",
                id: playlist.id
            }))
        } catch(err) {
            errors.push(err)
        }
        
    })

    if(errors.length > 0) {
        return Promise.reject("There were some errors. abort")
    }
   
    const responses : YTResponse<Playlist[]>[] = await Promise.all(promises)
    
    responses.forEach((resp : YTResponse<Playlist[]>) => {
        if(resp.notModified) {
            return
        }

        newPlaylists.push(...resp.response)
    })


    return newPlaylists
}