

export interface ThumbnailsData {
    url : string
    width : number
    height : number
}

interface Thumbnails {
    default : ThumbnailsData
    medium : ThumbnailsData
    high : ThumbnailsData
    standard : ThumbnailsData
    maxres : ThumbnailsData
}

export interface YTResponse<T> {
    notModified?: boolean
    metaData: MetaData
    response: T
}

export interface MetaData {
    etag : string
    channelId: string
    kind : "playlists" | "playlistItems"
    nextPageToken : string
    prevPageToken : string
    pageInfo : { totalResults : number, resultsPerPage: number}
}

export interface Playlist {
    etag : string
    id : string
    metaId: string
    publishedAt : string
    title : string
    description : string
    thumbnails : Thumbnails
    channelTitle : string
    channelId : string
}

export interface PlaylistItem {
    etag : string
    id : string
    playlistId : string
    channelId : string
    publishedAt : string
    title : string
    description : string
    channelTitle : string
    position : number
    thumbnails : Thumbnails
    videoId : string
}