import PlaylistApi from "./api/PlaylistApi";
import dataApi from "./api/DataApi";
import { ListOptions, ListResponse, ApiResponse, PlaylistResponse, IPlaylistApi } from "./api/types";
import { Playlist, YTResponse } from "./types";
import { transformPlaylistResponse } from "./helpers";

export const YTPlaylist = () => dataApi().playlist()