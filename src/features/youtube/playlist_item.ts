import {ListResponse, ApiResponse, PlaylistResponse, IPlaylistApi, PlaylistItemResponse, ItemListOptions } from "./api/types";
import { YTResponse, PlaylistItem } from "./types";
import { transformPlaylistItemResponse } from "./helpers";
import { YTPlaylist } from ".";

export const loadAllPlItems = async (options : ItemListOptions) : Promise<YTResponse<PlaylistItem[]>[]> => {
    let responses : YTResponse<PlaylistItem[]>[] = []
    const snippet : YTResponse<PlaylistItem[]> = await loadPlaylistItem(options)
    responses = responses.concat(snippet)
    if(snippet.metaData.nextPageToken != "") {
        options.pageToken = snippet.metaData.nextPageToken
        return responses.concat(await loadPlaylistItem(options))
    } else {
        return responses
    }
}

export const loadPlaylistItem = async (options : ItemListOptions) : Promise<YTResponse<PlaylistItem[]>> => {
    const playlistApi : IPlaylistApi = YTPlaylist()
    let plItems : PlaylistItem[] = []
    const jsonResponse : ApiResponse<ListResponse<PlaylistItemResponse>> = await playlistApi.listPlaylistItems(options)

    if(jsonResponse.isError || jsonResponse.data === undefined) {
        return Promise.reject(jsonResponse.errMessage)
    }

    const notModified = jsonResponse.errMessage == "304"

    const respData : ListResponse<PlaylistResponse> = jsonResponse.data


    plItems = jsonResponse.data.items.map<PlaylistItem>(resp => transformPlaylistItemResponse(resp, respData.etag))

    const result : YTResponse<PlaylistItem[]> = {
        notModified,
        response: plItems,
        metaData: {
            etag: respData.etag,
            channelId: options.channelId ? options.channelId : "",
            kind: "playlists",
            nextPageToken: respData.nextPageToken,
            prevPageToken: respData.prevPageToken,
            pageInfo: {
                totalResults: respData.pageInfo.totalResults,
                resultsPerPage: respData.pageInfo.resultsPerPage
            }
        }
    }

    return result
}
