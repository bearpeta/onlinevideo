import { ListOptions, ApiResponse, ListResponse, ItemListOptions, ApiOptions, PlaylistResponse, PlaylistItemResponse, IPlaylistApi } from "./types";
import { createPlaylistResponse, composeListParams, createListResponse, createPlaylistItemResponse } from "./helpers";

export default class PlaylistApi implements IPlaylistApi {

    _apiUrl : string
    _apiKey : string

    constructor(apiUrl : string, apiKey : string) {
        this._apiUrl = apiUrl
        this._apiKey = apiKey
    }

    _fetchData = (endpoint : string, opt : ApiOptions) : Promise<Response> => {

        const headers : HeadersInit_= new Headers( {
            "Accept-Encoding": "gzip",
            "User-Agent": "OnlineVideo (gzip)",
            "Accept": 'application/json',
        })

        if(opt.etag && opt.etag != '') {
            const encodedEtag = opt.etag//.replace(/\"/g, "")
            headers.set('If-None-Match', encodedEtag)
            delete opt.etag
        }

        let listParams = composeListParams(opt)

        return fetch(`${this._apiUrl}/${endpoint}?${listParams}&key=${this._apiKey}`, {
            method: "GET",
            headers   
          })
    }

    list = async (opt : ListOptions) : Promise<ApiResponse<ListResponse<PlaylistResponse>>> => {

        const etag = opt.etag
      
        const response : Response = await this._fetchData("playlists", opt)

        if(!response.ok) {
            return {
                isError: true,
                errMessage: response.status + ""
            }
        }

        // For some reasons (cache?) the fetch API always returns 200 even if there are no changes.. 
        // Because of that we compare the etag we sent with the received etag. if it is the same we mark the response wit an error message
        const errMessage = etag != undefined && etag == response.headers.get("etag") ? "304" : ""
       
        const json = await response.json()
        
        const listResponse : ListResponse<PlaylistResponse> = createListResponse<PlaylistResponse>(json, createPlaylistResponse)

        return {
            isError: false,
            errMessage: errMessage,
            data: listResponse
        }
    }

    listPlaylistItems = async (opt : ItemListOptions) : Promise<ApiResponse<ListResponse<PlaylistItemResponse>>> => {
        const etag = opt.etag
      
        const response : Response = await this._fetchData("playlistItems", opt)

        if(!response.ok) {
            return {
                isError: true,
                errMessage: response.status + ""
            }
        }

        const errMessage = etag != undefined && etag == response.headers.get("etag") ? "304" : ""
    
        const json = await response.json()
        const listResponse : ListResponse<PlaylistItemResponse> = createListResponse<PlaylistItemResponse>(json, createPlaylistItemResponse)

        return {
            isError: false,
            errMessage: errMessage,
            data: listResponse
        }

    }
}