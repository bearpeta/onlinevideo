import { PlaylistResponse, ApiOptions, PlaylistItemResponse, ListResponse } from "./types";

export const setValue = (value: any, defaultValue: any) => {
    return (value === undefined) ? defaultValue : value;
}

const _fillGeneralPlaylistData = (item : any) => ({
    kind: setValue(item.kind, ''),
    etag: setValue(item.etag, ''),
    id: setValue(item.id, ''),
    publishedAt: setValue((item.snippet || {}).publishedAt, ""),
    title: setValue((item.snippet || {}).title, ""),  
    description: setValue((item.snippet || {}).description, ""),
    channelTitle: setValue((item.snippet || {}).channelTitle, ""),
    channelId: setValue((item.snippet || {}).channelId, ""),
    thumbnails: setValue((item.snippet || {}).thumbnails, {})
})

export const createPlaylistResponse = (list : []) : PlaylistResponse[] => {
    const responses : PlaylistResponse[] = []
    if (list === undefined || list.length == 0) {
        return responses
    }

    list.forEach((item : any) => {
        //The syntax {...object} doesn't make anything else than merging the object into a new object. This way multiple objects can merged together into a new one.
        let playlist : PlaylistResponse = {..._fillGeneralPlaylistData(item)}

        responses.push(playlist)
    })

    return responses
}

export const createPlaylistItemResponse = (list : []) : PlaylistItemResponse[] => {
    const responses : PlaylistItemResponse[] = []
    list.forEach((item : any) => {
        let plItem : PlaylistItemResponse = {..._fillGeneralPlaylistData(item), ...{
            playlistId: setValue((item.snippet || {}).playlistId, ""),
            position: setValue((item.snippet || {}).position, ""),
            resourceId: {
                kind: setValue(((item.snippet || {}).resourceId || {}).kind, "youtube#video"),
                videoId: setValue(((item.snippet || {}).resourceId || {}).videoId, "")
            },
        }}

        responses.push(plItem)
    })

    return responses
}

export const createListResponse = <T>(json: any, cbListItems : (items: []) => T[]) : ListResponse<T> => ({
    etag: json.etag,
    nextPageToken: setValue(json.nextPageToken, ""),
    prevPageToken: setValue(json.prevPageToken, ""),
    pageInfo: {
        resultsPerPage: setValue((json.pageInfo || {}).resultsPerPage, 0),
        totalResults: setValue((json.pageInfo || {}).totalResults, 0)
    },
    items : cbListItems(json.items)
})

export const composeListParams = (opt : ApiOptions) : string => {
    let params = ""

    //if(opt.maxResults === undefined ) opt.maxResults = Env.get("YT_RESULTS_PER_CALL")

    for (const key in opt) {
        if (!opt.hasOwnProperty(key) || opt[key] === undefined) {
            continue 
        }
        
        params += `&${key}=${opt[key]}`
    }

    // we have to slice it because the first character is a '&' which shouldn't be there
    return params.slice(1)
}