import Env from "../../env/Env";
import PlaylistApi from "./PlaylistApi";
import { IPlaylistApi } from "./types";

const dataApi = () : DataApi => {
    return DataApi.getInstance()
}

export default dataApi

export class DataApi {
    static instance : DataApi | null = null

    apiUrl : string
    apiKey : string
    resultsPerCall : number

    static getInstance() : DataApi {
        if(DataApi.instance == null) {
            DataApi.instance = new DataApi()
        } 

        return DataApi.instance
    }
    
    constructor() {
        this.apiKey = Env.get("GOOGLE_API_KEY")
        this.apiUrl = Env.get("YT_DATA_API_URL")
        this.resultsPerCall = Env.get("YT_RESULTS_PER_CALL")
    }

    playlist = () : IPlaylistApi => {
        return new PlaylistApi(this.apiUrl, this.apiKey)
    }

}