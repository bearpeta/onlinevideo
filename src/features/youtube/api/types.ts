
/**
 * General Youtube DATA API types
 */
export interface ApiResponse<T> {
    isError : boolean
    errMessage : string
    data? : T
}

export interface ThumbnailsData {
    url : string
    width : number
    height : number
}

interface Thumbnails {
    default : ThumbnailsData
    medium : ThumbnailsData
    high : ThumbnailsData
    standard : ThumbnailsData
    maxres : ThumbnailsData
}

export interface ApiOptions {
    part : "snippet" | "contentDetail" | "id"
    pageToken? : string
    maxResults? : string
    etag? : string
    [key : string] : string | undefined
}

export interface ListResponse<T> {
    etag : string
    nextPageToken : string
    prevPageToken : string
    pageInfo : { totalResults : number, resultsPerPage: number}
    items : T[]
}


/**
 * Youtube Data Playlist API
 */

 export interface IPlaylistApi {
    list: (opt : ListOptions) => Promise<ApiResponse<ListResponse<PlaylistResponse>>>
    listPlaylistItems: (opt : ItemListOptions) => Promise<ApiResponse<ListResponse<PlaylistItemResponse>>>
 }

export interface PlaylistResponse {
    kind : string
    etag : string
    id : string
    publishedAt : string
    title : string
    description : string
    thumbnails : Thumbnails
    channelTitle : string
    channelId: string
}

export interface PlaylistItemResponse {
    kind : string
    etag : string
    id : string
    playlistId : string
    channelId : string
    publishedAt : string
    title : string
    description : string
    channelTitle : string
    position : number
    thumbnails : Thumbnails
    resourceId : {
        kind: string
        videoId : string
    }

}

export interface ListByChannelOptions extends ApiOptions {
    channelId: string
} 

export interface ListByIdOptions extends ApiOptions {
    id: string
} 

export type  ListOptions = ListByChannelOptions | ListByIdOptions

export interface ItemListOptions extends ApiOptions {
    playlistId : string
}