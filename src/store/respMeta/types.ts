import { Action } from 'redux';
import { MetaData } from '../../features/youtube/types';

export const ADD_RESP_META = 'ADD_RESP_META'
export const ADD_RESP_METAS = 'ADD_RESP_METAS'

/**
 * RESPONSE META DATA
 */
interface AddRespMetaAction extends Action {
  type: typeof ADD_RESP_META
  payload: {
    respMeta: MetaData
  }
}

interface AddRespMetasAction extends Action {
  type: typeof ADD_RESP_METAS
  payload: {
    respMetas: MetaData[]
  }
}

export type RespMetaActions = AddRespMetaAction | AddRespMetasAction

export interface RespMetaState {
    respMetas : MetaData[]
}