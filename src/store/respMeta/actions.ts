import { ADD_RESP_META, ADD_RESP_METAS, RespMetaActions } from "./types"
import { MetaData } from "../../features/youtube/types";

export const addRespMeta = (respMeta : MetaData) : RespMetaActions => ({
    type: ADD_RESP_META,
    payload: {respMeta}
})

export const addRespMetas = (respMetas : MetaData[]) : RespMetaActions => ({
    type: ADD_RESP_METAS,
    payload: {respMetas}
})