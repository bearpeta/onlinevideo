import { Reducer} from 'redux'
import { RespMetaState, RespMetaActions, ADD_RESP_META, ADD_RESP_METAS } from './types'
import { initRespMeta } from '../helper';

const RespMeta: Reducer<RespMetaState, RespMetaActions> = (state : RespMetaState = initRespMeta, action : RespMetaActions) : RespMetaState => {
    switch (action.type) {
        case ADD_RESP_META:
            return {...state, respMetas: [...state.respMetas, action.payload.respMeta]}
        case ADD_RESP_METAS:
            return {...state, respMetas: [...state.respMetas, ...action.payload.respMetas]}
        default:
            return state
    }
}

export {RespMeta}