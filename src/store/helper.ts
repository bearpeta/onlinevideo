import { PlaylistState, PlaylistItemState } from "./playlist/types";
import { AppState } from "./types";
import { RespMetaState } from "./respMeta/types";


export const initPlaylist : PlaylistState = {
    playlists: [],
    current_playling_id: ""
}

export const initPlaylistItem : PlaylistItemState = {
    items: []
}

export const initRespMeta : RespMetaState = {
    respMetas: []
}

export const initApp : AppState = {
    playlist: initPlaylist,
    playlistItem: initPlaylistItem,
    respMeta: initRespMeta
}