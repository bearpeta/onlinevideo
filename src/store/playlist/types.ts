import { Action } from 'redux';
import { PlaylistItem, Playlist } from '../../features/youtube/types';

export const ADD_PLAYLIST = 'ADD_PLAYLIST'
export const ADD_PLAYLISTS = 'ADD_PLAYLISTS'
export const SET_CURRENT_PLAYLIST_ID = 'SET_CURRENT_PLAYLIST_ID'
export const REPLACE_PLAYLISTS = 'REPLACE_PLAYLISTS'
export const ADD_PLAYLIST_ITEM = "ADD_PLAYLIST_ITEM"
export const ADD_PLAYLIST_ITEMS = "ADD_PLAYLIST_ITEMS"

/**
 * PLAYLIST
 */
interface AddPlaylistAction extends Action {
  type: typeof ADD_PLAYLIST
  payload: {
    playlist: Playlist
  }
}

interface AddPlaylistsAction extends Action {
  type: typeof ADD_PLAYLISTS
  payload: {
    playlists: Playlist[]
  }
}

interface ReplacePlaylistsAction extends Action {
  type: typeof REPLACE_PLAYLISTS
  payload: {
    playlists: Playlist[]
  }
}

interface SetCurrentPlaylistAction extends Action {
  type: typeof SET_CURRENT_PLAYLIST_ID
  payload: {
    id: string
  }
}

export type PlaylistActions = AddPlaylistAction | AddPlaylistsAction | ReplacePlaylistsAction | SetCurrentPlaylistAction

export interface PlaylistState {
    playlists : Playlist[]
    current_playling_id: string
}

/**
 * PLAYLIST ITEMS
 */

interface AddPlaylistItemAction extends Action {
  type: typeof ADD_PLAYLIST_ITEM
  payload: {
    item: PlaylistItem
  }
}

interface AddPlaylistItemsAction extends Action {
  type: typeof ADD_PLAYLIST_ITEMS
  payload: {
    items: PlaylistItem[]
  }
}

export type PlaylistItemActions = AddPlaylistItemAction | AddPlaylistItemsAction

export interface PlaylistItemState {
  items : PlaylistItem[]
}