import { Reducer} from 'redux'
import { PlaylistActions, ADD_PLAYLIST, ADD_PLAYLISTS, PlaylistState, PlaylistItemState, PlaylistItemActions, ADD_PLAYLIST_ITEM, ADD_PLAYLIST_ITEMS, REPLACE_PLAYLISTS, SET_CURRENT_PLAYLIST_ID } from './types'
import { initPlaylist, initPlaylistItem } from '../helper';
import { PlaylistItem as PlaylistItemType, Playlist as PlaylistType } from '../../features/youtube/types';

const Playlist: Reducer<PlaylistState, PlaylistActions> = (state : PlaylistState = initPlaylist, action : PlaylistActions) : PlaylistState => {
    switch (action.type) {
        case ADD_PLAYLIST:
            return {...state, playlists: [...state.playlists, action.payload.playlist]}
        case ADD_PLAYLISTS:
            return {...state, playlists: [...state.playlists, ...action.payload.playlists]}
        case REPLACE_PLAYLISTS:
            return {...state, playlists: action.payload.playlists}
        case SET_CURRENT_PLAYLIST_ID:
            return {...state, current_playling_id: action.payload.id}
        default:
            return state
    }
}

const PlaylistItem : Reducer<PlaylistItemState, PlaylistItemActions> = (state : PlaylistItemState = initPlaylistItem, action : PlaylistItemActions) : PlaylistItemState => {
    switch (action.type) {
        case ADD_PLAYLIST_ITEM:
            return {...state, items: _add(state.items, [action.payload.item])}
        case ADD_PLAYLIST_ITEMS:
                return {...state, items: _add(state.items, action.payload.items)}
        default:
            return state
    }
}


const _add = <T extends PlaylistType | PlaylistItemType>(currentItems : T[], newItems : T[]) :  T[] => {
    newItems.forEach(newItem => {
        const idx : number = currentItems.findIndex(currentItem => currentItem.id == newItem.id)
        if(idx === -1) {
            currentItems.push(newItem)
            return
        }

        currentItems[idx] = newItem
    })

    return currentItems
}

export {Playlist, PlaylistItem}