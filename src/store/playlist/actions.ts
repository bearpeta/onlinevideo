import { PlaylistActions, ADD_PLAYLIST, ADD_PLAYLISTS, PlaylistItemActions, ADD_PLAYLIST_ITEM, ADD_PLAYLIST_ITEMS, REPLACE_PLAYLISTS, SET_CURRENT_PLAYLIST_ID } from "./types"
import { Playlist, PlaylistItem } from "../../features/youtube/types";

export const addPlaylist = (playlist : Playlist) : PlaylistActions => ({
    type: ADD_PLAYLIST,
    payload: {playlist}
})

export const addPlaylists = (playlists : Playlist[]) : PlaylistActions => ({
    type: ADD_PLAYLISTS,
    payload: {playlists}
})

export const replacePlaylists = (playlists : Playlist[]) : PlaylistActions => ({
    type: REPLACE_PLAYLISTS,
    payload: {playlists}
})

export const setCurrentPlaylingPlaylist = (id : string) : PlaylistActions => ({
    type: SET_CURRENT_PLAYLIST_ID,
    payload: {id}
})

export const addItem = (item : PlaylistItem) : PlaylistItemActions => ({
    type: ADD_PLAYLIST_ITEM,
    payload: {item}
})

export const addItems = (items : PlaylistItem[]) : PlaylistItemActions => ({
    type: ADD_PLAYLIST_ITEMS,
    payload: {items}
})