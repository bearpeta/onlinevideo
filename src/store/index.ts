import { createStore, Reducer, combineReducers, applyMiddleware, Store } from 'redux'
import thunk from 'redux-thunk'
import { Playlist, PlaylistItem } from './playlist/reducers';
import { RespMeta } from './respMeta/reducers';
import { persistAfter } from './middleware';
import { AppState, StoreActions} from './types';


export const rootReducer: Reducer<AppState> = combineReducers<AppState>({
    playlist: Playlist,
    playlistItem: PlaylistItem,
    respMeta: RespMeta
});

const store: {store : Store<AppState, StoreActions> | null} = {
    store: null
}


export default (initialState : AppState | {} = {}) : Store<AppState, StoreActions> => {
    if(store.store == null) {
        store.store = createStore<AppState, StoreActions, {}, AppState>(rootReducer,  initialState, applyMiddleware(thunk, persistAfter))
    }
    
    return store.store
}


// export type AppState = ReturnType<typeof rootReducer>
