import {Dispatch, AnyAction, Middleware, MiddlewareAPI} from 'redux' 
import { ADD_PLAYLIST, ADD_PLAYLISTS, ADD_PLAYLIST_ITEM, ADD_PLAYLIST_ITEMS, REPLACE_PLAYLISTS } from './playlist/types';
import { StoreActions, AppState, PLAYLIST_STATE_KEY, PLAYLIST_ITEM_STATE_KEY, RESP_META_STATE_KEY } from './types';
import { AsyncStorage } from 'react-native'
import { ADD_RESP_META, ADD_RESP_METAS } from './respMeta/types';

const persistAfter : Middleware<{}, AppState> = (store: MiddlewareAPI<Dispatch<AnyAction>,AppState>) => (next: Dispatch<StoreActions>) => (action: StoreActions) => {
    // let the action go to the reducers
    let result = next(action);

    let state = null
    let storeKey = ""


    switch(action.type) {
        case ADD_PLAYLIST:
        case ADD_PLAYLISTS:
        case REPLACE_PLAYLISTS:
            state = store.getState().playlist
            storeKey = PLAYLIST_STATE_KEY
            break
        case ADD_PLAYLIST_ITEM:
        case ADD_PLAYLIST_ITEMS:
            state = store.getState().playlistItem
            storeKey = PLAYLIST_ITEM_STATE_KEY
            break
        case ADD_RESP_META:
        case ADD_RESP_METAS:
            state = store.getState().respMeta
            storeKey = RESP_META_STATE_KEY
            break
    }

    if(state != null && storeKey != "") {
        AsyncStorage.setItem(storeKey, JSON.stringify(state)).catch((error) => {
            console.log("AsyncStorage set item "+ action.type + " failed: "+ error)
        })    
    }

    return result
};

export {persistAfter} 