import { PlaylistActions, PlaylistState, PlaylistItemActions, PlaylistItemState } from "./playlist/types";
import { RespMetaActions, RespMetaState } from "./respMeta/types";

export interface AppState {
    playlist: PlaylistState
    playlistItem: PlaylistItemState
    respMeta: RespMetaState
}

export type StoreActions = PlaylistActions | PlaylistItemActions | RespMetaActions

export const PLAYLIST_STATE_KEY = "playlist_state"
export const PLAYLIST_ITEM_STATE_KEY = "playlist_item_state"
export const RESP_META_STATE_KEY = "resp_meta_state"

export const PERSIST_KEYS = [
    PLAYLIST_STATE_KEY,
    PLAYLIST_ITEM_STATE_KEY,
    RESP_META_STATE_KEY
]