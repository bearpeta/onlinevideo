import { AsyncStorage } from "react-native";
import { PLAYLIST_STATE_KEY, AppState, PLAYLIST_ITEM_STATE_KEY, RESP_META_STATE_KEY } from "../types";
import { initApp } from "../helper";


export const loadPersistedState = async () : Promise<AppState> => {
    let initState : AppState = initApp

    const persistedPlaylists = await AsyncStorage.getItem(PLAYLIST_STATE_KEY)
    if(persistedPlaylists) {
        initState.playlist = JSON.parse(persistedPlaylists)
    }

    const persistedPlaylistItems = await AsyncStorage.getItem(PLAYLIST_ITEM_STATE_KEY)
    if(persistedPlaylistItems) {
        initState.playlistItem = JSON.parse(persistedPlaylistItems)
    }

    const persistedRespMetas = await AsyncStorage.getItem(RESP_META_STATE_KEY)
    if(persistedRespMetas) {
        initState.respMeta = JSON.parse(persistedRespMetas)
    }
    return initState
}