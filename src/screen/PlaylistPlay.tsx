import * as React from 'react'
import * as Redux from 'redux'
import { StyleSheet, Dimensions, Text, View, FlatList, WebView, Platform, RefreshControl } from 'react-native'
import { connect, MapStateToPropsParam, MapDispatchToPropsParam } from 'react-redux'
import RootView from '../view/RootView'
import { AppState } from '../store/types'
import { NavigationProps } from './helper'
import { addItems } from '../store/playlist/actions'
import { YTResponse, PlaylistItem } from '../features/youtube/types';
import { loadAllPlItems } from '../features/youtube/playlist_item';
import PlaylistItemElement from '../view/PlaylistItemElement';

const {width, height} = Dimensions.get("window")
const playerViewWidth = (width -40)
const playerViewHeight = playerViewWidth / 16 * 9

// Home component specific state
interface PlaylistPlayState {
    playingItemVideoId : string
    isDownloading : boolean
}

// Home component specific props
interface PlaylistPlayProps extends NavigationProps {
}

// Redux state props
interface StateProps {
    currentPlaylistId: string,
    playlistItems: PlaylistItem[],
}
     
// Redux actions calls props
interface DispatchProps {
    addPlaylistItems : (arg :PlaylistItem[]) => void
}
 
type AllProps = StateProps & DispatchProps & PlaylistPlayProps

class PlaylistPlay extends React.Component<AllProps, PlaylistPlayState> {
    state : PlaylistPlayState = {
        playingItemVideoId: "",
        isDownloading: false
    }

    videoPlayer : WebView | null = null

    readonly gridColumns : number = 1

    componentDidMount() {
        console.log(`PlaylistPlay componentDidMount: called`)
        if(this.props.currentPlaylistId == "") {
            console.log("For some reason the PlaylistPlay screen should be shown but no currentPlaylistId is set -> navigate to Home")
            this.props.navigation.navigate("Home")
            return
        }
        if(this.props.playlistItems.length > 0 && this.state.playingItemVideoId == "") {
            console.log(`PlaylistPlay componentDidMount: there are some playlist items. Take first one`)
            this.setState({playingItemVideoId: this.props.playlistItems[0].videoId})
        }

        console.log(`PlaylistPlay componentDidMount: download items for playlist ID ${this.props.currentPlaylistId}`)
        this.downloadItemsForPlaylist(this.props.currentPlaylistId, this.props.addPlaylistItems)
    } 

    // TODO : It shouldn't download the items everytime from new
    downloadItemsForPlaylist = (id : string, dispatch : (arg :PlaylistItem[]) => void) => {
        console.log("PlaylistPlay downloadItemsForPlaylist: called")
        this.setState({isDownloading: true})
        loadAllPlItems({part: "snippet", playlistId: id}).then((resp : YTResponse<PlaylistItem[]>[]) => {
            const items : PlaylistItem[] = []
            resp.forEach((ytResp) => items.push(...ytResp.response))
            dispatch(items)
            this.setState({isDownloading: false})
        }).catch(err => {
            console.log(`PlaylistPlay downloadItemsForPlaylist: error occured: ${err}`)
            this.setState({isDownloading: false})
        })
    }


    onShouldStartLoadWithRequest = (navigator: { url: string} ) : boolean => {
        if (navigator.url.indexOf('embed') !== -1) {
            console.log(`PlaylistPlay onShouldStartLoadWithRequest: navigator url has embed in it: ${navigator.url}`)
            return true;
        } else {
            console.log(`PlaylistPlay onShouldStartLoadWithRequest: navigator url hasn't embed in it: ${navigator.url}`)
            this.videoPlayer!.stopLoading(); //Some reference to your WebView to make it stop loading that URL
            return false;
        }    
    }

    onShouldStartLoadWithRequestAndroid = (navigator : {url?: string | undefined}) : void => {
        console.log("PlaylistPlay onShouldStartLoadWithRequestAndroid: called")
        if(navigator.url === undefined || typeof(navigator.url) != "string") {
            return
        }
        console.log(`PlaylistPlay onShouldStartLoadWithRequestAndroid: navigator url is set: ${navigator.url}`)

        // @ts-ignore
        this.onShouldStartLoadWithRequest(navigator)
    }

    _getWebViewHtml = () => {
        if(this.state.playingItemVideoId === "") {
            return (
                <View style={customStyles.webview}>
                    <Text style={{fontWeight:"700", color: "red"}}>No Video selected</Text>
                </View>
            )
        }

        let source
        if(true ) {//Platform.OS === "ios") {
            source = { uri: `https://www.youtube.com/embed/${this.state.playingItemVideoId}?version=3&enablejsapi=0&rel=0&autoplay=1&controls=1&rel=0&playsinline=1`}
        } else {
            source = {html: `<iframe id="ytplayer" src="https://www.youtube.com/embed/${this.state.playingItemVideoId}?version=3&autoplay=1&controls=1&rel=0&playsinline=1" frameborder="0" style="height:${playerViewHeight};width:${playerViewWidth};" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen"></iframe>`}
        } 

        return (
        <WebView
            style={customStyles.webview}
            ref={(ref) => { this.videoPlayer = ref;}}
            originWhitelist={["*"]}
            scalesPageToFit={false}  
            scrollEnabled={false}
            allowsInlineMediaPlayback={true}
            source={source} 
            javaScriptEnabled={true}
            domStorageEnabled={true}
            onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest} //for iOS
            onNavigationStateChange ={this.onShouldStartLoadWithRequestAndroid} //for android
        />
        )
    }

    render() {
            
        return (
        <RootView style={customStyles.root}>
            {this._getWebViewHtml()}

            <FlatList
                data={this.props.playlistItems}
                style={customStyles.list}
                horizontal={false}
                refreshControl={
                    <RefreshControl
                      refreshing={this.state.isDownloading}
                      onRefresh={() => this.downloadItemsForPlaylist(this.props.currentPlaylistId, this.props.addPlaylistItems)}
                    />
                  }                numColumns={this.gridColumns}
                keyExtractor={(item, idx) => item.id}
                ItemSeparatorComponent={() => <View style={{ width: "100%", margin: 12, borderColor: "grey", borderWidth:0.5}}/>}
                renderItem={({ item }) => (
                    <PlaylistItemElement 
                        item={item} 
                        onPress={() => this.setState({playingItemVideoId: item.videoId})} 
                    />
                )}
            />

        </RootView>
        )
    }
}

const customStyles = StyleSheet.create({
    root: {

    },
    webview: {
        marginTop: (Platform.OS == 'ios') ? 20 : 0,
        marginRight: 20,
        marginLeft: 20,
        marginBottom: 20,
        width: playerViewWidth,
        height: playerViewHeight, 
        justifyContent: "center",
        alignItems: "center"
    
    },
    list : {
        flex: 1,
        width: "100%",
        
    }
})

const mapStateToProps : MapStateToPropsParam<StateProps, AllProps, AppState> = (state : AppState) : StateProps => {
    return {
        currentPlaylistId: state.playlist.current_playling_id,
        playlistItems: state.playlistItem.items.filter(item => item.playlistId == state.playlist.current_playling_id)
    }
}

const mapDispatchToProps : MapDispatchToPropsParam<any, AllProps> = (dispatch: Redux.Dispatch<any>) : DispatchProps => {
    return {
         addPlaylistItems: (playlistItems : PlaylistItem[]) => dispatch(addItems(playlistItems))
    }
}

export default connect<StateProps, DispatchProps, AllProps, AppState>(mapStateToProps, mapDispatchToProps)(PlaylistPlay)
