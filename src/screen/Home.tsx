import * as React from 'react'
import * as Redux from 'redux'
import { StyleSheet, Platform, View, FlatList, Dimensions} from 'react-native'
import { connect, MapStateToPropsParam, MapDispatchToPropsParam } from 'react-redux'
import RootView from '../view/RootView'
import { AppState } from '../store/types'
import { NavigationProps } from './helper'
import { setCurrentPlaylingPlaylist } from '../store/playlist/actions'
import { Playlist } from '../features/youtube/types';
import PlaylistElement from '../view/PlaylistElement';


// Home component specific state
interface HomeState{
}

// Home component specific props
interface HomeProps extends NavigationProps {
}

// Redux state props
interface StateProps {
  playlists: Playlist[],
}
     
// Redux actions calls props
interface DispatchProps {
  setPlayingPlaylist: (id : string) => void
}
 
type AllProps = StateProps & DispatchProps & HomeProps

const gridColumns = 2
const {width, height} = Dimensions.get("window")
const playerViewWidth = (width / gridColumns) - 20
const playerViewHeight = playerViewWidth / 16 * 9


class Home extends React.Component<AllProps, HomeState> {
    state : HomeState = {
    }

    readonly gridColumns : number = gridColumns

    
     
    render() {
        
        return (
            <RootView style={customStyles.root}>
            <FlatList
            data={this.props.playlists}
            style={customStyles.list}
    
            numColumns={this.gridColumns}
            keyExtractor={(item, idx) => item.id}
            renderItem={({ item }) => (
                <View style={{ flex: 1, flexDirection: 'column', margin: 5}}>
                    <PlaylistElement width={playerViewWidth - 10} item={item} onPress={() => {
                        this.props.setPlayingPlaylist(item.id)
                        this.props.navigation.navigate("PlaylistPlay")
                    }} />  
                </View>
        )}
      />

            </RootView>
        )
    }
}

 /* 
 <TouchableWithoutFeedback onPress={() => {
                        this.props.setPlayingPlaylist(item.id)
                        this.props.navigation.navigate("PlaylistPlay")
                    }} style={{width: playerViewWidth, height: playerViewHeight}}>
                   <Image resizeMode="cover" style={{width: item.thumbnails.default.width, height: item.thumbnails.default.height}} source={{uri: item.thumbnails.default.url}} />

                    
                    </TouchableWithoutFeedback>
                    */

//                    <Image resizeMode="cover" style={{width: item.thumbnails.default.width, height: item.thumbnails.default.height}} source={{uri: item.thumbnails.default.url}} />

const customStyles = StyleSheet.create({
    root: {
       
    },
    list : {
        width: "100%",
        height: "100%",
        
    },
    webview: {
        marginTop: (Platform.OS == 'ios') ? 20 : 0,
        marginRight: 20,
        marginLeft: 20,
        marginBottom: 20,
        width: playerViewWidth,
        height: playerViewHeight, 
    
    }
})

const mapStateToProps : MapStateToPropsParam<StateProps, AllProps, AppState> = (state : AppState) : StateProps => {
    return {
        playlists: state.playlist.playlists.filter(playlist => playlist.title.indexOf("Muskoka") != -1),
    }
}

const mapDispatchToProps : MapDispatchToPropsParam<any, AllProps> = (dispatch: Redux.Dispatch<any>) : DispatchProps => {
    return {
        setPlayingPlaylist: (id : string) => dispatch(setCurrentPlaylingPlaylist(id)),
    }
}

export default connect<StateProps, DispatchProps, AllProps, AppState>(mapStateToProps, mapDispatchToProps)(Home)
