import * as React from 'react'
import { StyleSheet, Platform, Image, Text, View, Button } from 'react-native'
import { NavigationScreenProp } from 'react-navigation';

type HomeProps = {
    navigation: NavigationScreenProp<any,any>
  }

type HomeState = {

}
export default class Setting extends React.Component<HomeProps, HomeState> {
    state : HomeState = {
        currentUser: null
    }

    componentDidMount() {
    } 

     
    render() {
    return (
        <View style={styles.container}>
            <Text>
            </Text>
            <Button title="Logout" onPress={() => {}} />
        </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})