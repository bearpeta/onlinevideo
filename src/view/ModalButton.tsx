import * as React from 'react'
import {Component} from 'react'
import {Text, TouchableOpacity, StyleSheet, StyleProp, ViewStyle} from 'react-native'

type ModalButtonProps = {
  onPress?: () => void,
  btnText?: number | string
} & Partial<DefaultProps>

type DefaultProps = Readonly<typeof defaultProps>

const defaultProps = {
  styles: {} as StyleProp<ViewStyle>
}

export default class ModalButton extends Component<ModalButtonProps> {
  static defaultProps = defaultProps

  render() {
    return (
        <TouchableOpacity style={[customStyles.button, this.props.styles]}
        onPress={this.props.onPress}>
            <Text style={customStyles.buttonText}>{this.props.btnText}</Text>
        </TouchableOpacity>
    ); 
  }
}

const customStyles = StyleSheet.create({
    button: {
      backgroundColor: 'white',
      padding: 12,
      justifyContent: 'center',
      alignItems: 'center',
      borderColor: 'rgba(0, 0, 0, 0.1)',
     
    },
    buttonText: {
      fontSize: 12,
      color: "#fffff",
      fontWeight: "bold"
    }
});