import * as React from 'react'
import {Component}  from 'react'
import {View, SafeAreaView, StyleSheet} from 'react-native'

type RootViewProps = {
  style : object
}

export default class RootView extends Component<RootViewProps> {
  render() {
    return (
      <SafeAreaView style={customStyles.safeAreaView}>

      <View style={[customStyles.rootView, this.props.style]}>
        { this.props.children }
      </View>
      </SafeAreaView>
    ); 
  }
}

const customStyles = StyleSheet.create({
    safeAreaView: {
      flex: 1,
      backgroundColor: '#eceff1',
    },
    rootView: {
      width: "100%",
      height: "100%",
      padding: 20,
      backgroundColor: '#eceff1',
      alignItems: 'center',
      justifyContent: 'center',
    }

});