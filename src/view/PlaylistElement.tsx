import * as React from 'react'
import {Component} from 'react'
import {Text, TouchableOpacity, StyleSheet, StyleProp, ViewStyle, View, Image} from 'react-native'
import { Playlist } from '../features/youtube/types';

type PlaylistElementProps = {
    item: Playlist
    onPress: () => void
    width: number
} & Partial<DefaultProps>

type DefaultProps = Readonly<typeof defaultProps>

const customStyles = StyleSheet.create({
    buttonContainer: {
        flex: 1,
        width: "100%",

    },
    subContainer: {
       // borderColor: "black",
       // borderWidth: 2,
        flex: 1, 
        flexDirection: 'column', 
        padding: 1,
        alignContent: "center"
    },
    bottomContainer: { 
        //borderColor: "red",
        //borderWidth: 2,
        width: "100%",
        flexDirection: 'column',
        padding: 1,
        paddingLeft: 10,
        justifyContent:"center", 
        alignContent:"center"
    },
    topContainer: {      
        //borderColor: "green",
        //borderWidth: 2,
        aspectRatio:1.56,
        width: "100%",
        flexDirection: 'column',
        padding: 1,
        justifyContent:"center", 
        alignContent:"center"
    },
    thumbnail: {
        width: "100%",
        height: undefined,
        aspectRatio:1.56
    },
    titleText: {
        fontWeight: "700", 
        textAlign:"center",
        marginBottom: 5,
    },
    descriptionText: {

    }
});

const defaultProps = {
    styles: customStyles as StyleProp<ViewStyle>
}

export default class PlaylistElement extends Component<PlaylistElementProps> {
  static defaultProps = defaultProps

  render() {
    const {item, onPress, width} = this.props
    const defStyles = defaultProps.styles as typeof customStyles
    
    return (
        <TouchableOpacity style={[defStyles.buttonContainer, {width: width}]} onPress={onPress}>
        <View style={[defStyles.subContainer]}>
            <View style={defStyles.topContainer}>
                <Image resizeMode="cover" style={defStyles.thumbnail} source={{uri: item.thumbnails.default.url}} />
            </View>
            <View style={defStyles.bottomContainer}>
                <Text style={defStyles.titleText}>{item.title}</Text>
                <Text style={defStyles.descriptionText}>{item.description}</Text>
            </View>
            
        </View></TouchableOpacity>
    ); 
  }
}

