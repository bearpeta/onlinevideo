import * as React from 'react'
import {Component} from 'react'
import {View, StyleSheet, Alert, Dimensions} from 'react-native';
import Modal from 'react-native-modal';
import ModalButton from './ModalButton';

const dimens = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height
}

type ModalViewProps = {
  isVisible: boolean,
  
} & Partial<DefaultProps>

type DefaultProps = Readonly<typeof defaultProps>

const defaultProps = {
  showButtons: true as boolean,
  contentView: (<View/>) as JSX.Element,
  onCancelBtnPress: (() => Alert.alert("Press function on Cancel button not defined.")) as () => void,
  onOkBtnPress: (() => Alert.alert("Press function on OK button not defined.")) as () => void,
  cancelBtnText: "Cancel" as string | number,
  okBtnText: "OK" as string | number,
}

export default class ModalView extends Component<ModalViewProps> {
  static defaultProps = defaultProps
  
  showModalButtons() {
    if(!this.props.showButtons) {
      return;
    } 

    return (
    <View style={customStyles.buttonContainer}>
      <ModalButton 
        styles={customStyles.stdButton} 
        onPress={this.props.onCancelBtnPress} 
        btnText={this.props.cancelBtnText}
      />
      <View style={{width: "5%"}} />
      <ModalButton 
        styles={customStyles.stdButton} 
        onPress={this.props.onOkBtnPress} 
        btnText={this.props.okBtnText}
      />
    </View>);
  }

  render() {
    return (
      <Modal isVisible={this.props.isVisible}>
        <View style={customStyles.modalContent}>
            
          {this.props.contentView}

          {this.showModalButtons()}
        </View>
          
      </Modal>
    ); 
  }
}

const customStyles = StyleSheet.create({
    modalContent: {
      width: "100%",
      height: dimens.height*0.55,
      backgroundColor: "#fdfdf", 
      padding: 20,
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: 20,
      borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    buttonContainer: {
      width:"100%",
      height: "30%",
      marginBottom: -10,
      padding: 15,
      flexDirection: 'row',
      flexWrap: "wrap",      
      justifyContent: 'space-between',
    },
    stdButton: {
      width:"45%",
      height: "100%",
      borderRadius: Math.round(Dimensions.get('window').height) / 2
    }
});