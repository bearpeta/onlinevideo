import * as React from 'react'
import {Component} from 'react'
import {Text, TouchableOpacity, StyleSheet, StyleProp, ViewStyle, View, Image} from 'react-native'
import { PlaylistItem } from '../features/youtube/types';

type PlaylistItemElementProps = {
    item: PlaylistItem
    onPress: () => void,
} & Partial<DefaultProps>

type DefaultProps = Readonly<typeof defaultProps>

const customStyles = StyleSheet.create({
    buttonContainer: {
        flex: 1,
        width: "100%"
    },
    subContainer: {
        
        flex: 1, 
        flexDirection: 'row', 
        padding: 1,
        alignContent: "center"
    },
    rightContainer: { 
        
        height: "100%",
        width: "70%",
        flexDirection: 'column',
        padding: 1,
        paddingLeft: 10,
        justifyContent:"center", 
        alignContent:"center"
    },
    leftContainer: {      

        height: "100%",
        width: "30%",
        flexDirection: 'column',
        padding: 1,
        justifyContent:"center", 
        alignContent:"center"
    },
    thumbnail: {
        width: "100%",
        height: undefined,
        aspectRatio:1.56
    },
    titleText: {
        fontWeight: "700", 
        textAlign:"center",
        marginBottom: 5,
    },
    descriptionText: {

    }
});

const defaultProps = {
    styles: customStyles as StyleProp<ViewStyle>
}

export default class PlaylistItemElement extends Component<PlaylistItemElementProps> {
  static defaultProps = defaultProps

  render() {
    const {item, onPress} = this.props
    const styles = defaultProps.styles as typeof customStyles

    return (
        <TouchableOpacity style={styles.buttonContainer} onPress={onPress}>
        <View style={styles.subContainer}>
            <View style={styles.leftContainer}>
                <Image resizeMode="contain" style={styles.thumbnail} source={{uri: item.thumbnails.default.url}} />
            </View>
            <View style={styles.rightContainer}>
                <Text style={styles.titleText}>{item.title}</Text>
                <Text style={styles.descriptionText}>{item.description}</Text>
            </View>
            
        </View></TouchableOpacity>
    ); 
  }
}

