import * as React from 'react'
import * as Redux from 'redux'
import {Provider} from 'react-redux'
import { SplashScreen } from 'expo'
import {createStackNavigator, createAppContainer } from 'react-navigation'
import Home from './screen/Home'
import Setting from './screen/Setting'
import Env from './features/env/Env'
import { loadPlaylists, checkForPlaylistUpdate } from './features/youtube/playlist'
import configureStore from './store'
import { AppState, StoreActions } from './store/types'
import { addPlaylists, replacePlaylists } from './store/playlist/actions'
import { View } from 'react-native';
import { loadPersistedState } from './store/init';
import { Playlist, YTResponse, MetaData } from './features/youtube/types';
import { addRespMetas } from './store/respMeta/actions';
import { getIdsFromChannels } from './features/youtube/playlist';
import PlaylistPlay from './screen/PlaylistPlay';

 
const mainNavigator = createStackNavigator({
    Home: Home,
    PlaylistPlay: PlaylistPlay,
    Setting: Setting,
  }, {
    initialRouteName:"Home"
  })

const AppContainer = createAppContainer(mainNavigator)

// App component specific props
interface AppProps {
};

// App component specific state
interface AppLoadingState {
    storeRehydrated: boolean
    isReady: boolean
}


export default class App extends React.Component<AppProps, AppLoadingState> {

  state = {
    storeRehydrated: false,
    isReady: false,
  } 

  isLoading = false

  componentDidMount() {
    // TODO : add logic to catch method
    this._prepareApp()
    .then(() =>this.setState({isReady: true}))
    .catch((error) => {
      console.log("App PREPARE FAILED: " + error)
      this.setState({isReady: true})
    })
  } 

  store : Redux.Store<AppState, StoreActions> | null = null

  render() {
    SplashScreen.preventAutoHide()

    if(this.state.isReady && this.state.storeRehydrated && this.store) {
      SplashScreen.hide()
      return <Provider store={this.store}><AppContainer/></Provider>
    }

    return (<View/>)    
  }

  _getPlaylistsFirstTime = async (store : Redux.Store<AppState, StoreActions>) => {
    const playlistIds : string[] = await getIdsFromChannels()

    
    const ytPlPromises : Promise<YTResponse<Playlist[]>>[] = []

    try {
      playlistIds.forEach(playlistId => {
        ytPlPromises.push(loadPlaylists({id: playlistId, part: "snippet"}))
      })

    } catch (error) {
      return Promise.reject(error)
    }

    const ytPlaylistResponses : YTResponse<Playlist[]>[] = await Promise.all(ytPlPromises)

    const playlists : Playlist[] = []
    const metaDatas : MetaData[] = []

    ytPlaylistResponses.forEach(channelResponses => {
      if(channelResponses.notModified) {
        return
      }
      playlists.push(... channelResponses.response)
      metaDatas.push(channelResponses.metaData)
    })

    store.dispatch(addPlaylists(playlists))
    store.dispatch(addRespMetas(metaDatas))
  }

  _onStoreInitialized = async (store : Redux.Store<AppState, StoreActions>) : Promise<void> => {
//AsyncStorage.removeItem(PLAYLIST_ITEM_STATE_KEY)

    let currentPlaylists : Playlist[] = store.getState().playlist.playlists
    if(currentPlaylists.length == 0) {
      console.log("No playlists local, get all from youtube for the first time")
      await this._getPlaylistsFirstTime(store)
      this.setState({storeRehydrated: true})
      return
    }

    const newPlaylists : Playlist[] = []
    try {
      newPlaylists.push(...await checkForPlaylistUpdate(currentPlaylists))

    } catch (error) {
      return Promise.reject(error)
    }

    if(newPlaylists.length == 0) {
      this.setState({storeRehydrated: true})
      return
    }

    const samePlaylists = currentPlaylists.filter(currPlaylist => {
      return newPlaylists.findIndex(newPlaylist => newPlaylist.id == currPlaylist.id) == -1
    })

    const newPlaylistState = [...samePlaylists, ...newPlaylists]

    store.dispatch(replacePlaylists(newPlaylistState))
    this.setState({storeRehydrated: true})
  }

  _prepareApp = async () : Promise<void> => {
    
    await Env.load()
    const initState : AppState = await loadPersistedState()
    this.store = configureStore(initState)
    await this._onStoreInitialized(this.store)
  
    /*  
    const nextPageToken : string = (json || {}).nextPageToken;
    if(nextPageToken !== undefined && nextPageToken.trim() != "") {
      let jsonResponse = await playlistApi.list({channelId: channelIds[0], part: "snippet", pageToken: nextPageToken})
    } 
    */

    //console.log(JSON.stringify(json, null, "    ") )

    return

    //const images :number[] = [];


    /** 
    Object.keys(ImageSources).forEach(key => {
      var picObj = ImageSources[key]

      if(Object.prototype.toString.call(picObj.uri) == "[object Object]") {
        return Object.keys(picObj.uri).forEach(key => images.push(picObj.uri[key]))
      }
      return images.push(picObj.uri) 
    });

    const cacheImages = images.map((image) => Asset.loadAsync(image))

    return Promise.all(cacheImages)*/

  }
}